//
//  TestUserService.m
//  TestComposition
//
//  Created by Vladimir Obrizan on 12/7/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "UserService.h"
#import "BackendService.h"
#import "User.h"
#import <OCMock/OCMock.h>

@interface TestUserService : XCTestCase

@end

@implementation TestUserService

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testUserService {
    
    id classMock = OCMClassMock([BackendService class]);
    
    OCMStub([classMock new]).andReturn(classMock);
    OCMStub([classMock createUser:[OCMArg any]]);
    
    UserService *us = [UserService new];
    
    [us createUser:@"obrizan" password:@"asdf"];
    
    OCMArg * check = [OCMArg checkWithBlock:^BOOL(id obj)
             {
                 User *u = (User *)obj;
                 return [u.login isEqualToString:@"obrizan"] && [u.password isEqualToString:@"asdf"];
             }];
    
    OCMVerify([classMock createUser:check]);
}



@end
