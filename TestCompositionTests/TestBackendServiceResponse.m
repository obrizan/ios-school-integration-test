//
//  TestBackendServiceResponse.m
//  TestComposition
//
//  Created by Vladimir Obrizan on 12/8/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import <XCTest/XCTest.h>

#import <OCMock/OCMock.h>

#import "BackendService.h"

@interface TestBackendServiceResponse : XCTestCase

@end

@implementation TestBackendServiceResponse

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSuccessfullResponse {
    
    // 1. Setup.
    id userDefaultMock = OCMClassMock([NSUserDefaults class]);
    OCMStub([userDefaultMock standardUserDefaults]).andReturn(userDefaultMock);
    OCMStub([userDefaultMock setObject:[OCMArg any] forKey:[OCMArg any]]);
    
    id notificationMock = OCMClassMock([NSNotificationCenter class]);
    OCMStub([notificationMock defaultCenter]).andReturn(notificationMock);
    OCMStub([notificationMock postNotificationName:[OCMArg any] object:[OCMArg any]]);
    
    BackendService *backend = [BackendService new];
    
    NSDictionary *body = @{
                           @"access_token": @"123"
                           };
    
    NSError *e = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:body options:0 error:&e];
    
             //
    [backend connection:nil didReceiveResponse:nil];
    [backend connection:nil didReceiveData:data];
    [backend connectionDidFinishLoading:nil];
            
    // Verify.
    OCMVerify([userDefaultMock setObject:[OCMArg isEqual:@"123"]
                                  forKey:[OCMArg isEqual:@"access_token"]]);
    
    OCMVerify([notificationMock postNotificationName:[OCMArg isEqual:@"register"]
                                              object:[OCMArg isEqual:@"123"]]);
    
}


-(void)testIncorrectResponse {
    
    id notificationMock = OCMClassMock([NSNotificationCenter class]);
    OCMStub([notificationMock defaultCenter]).andReturn(notificationMock);
    OCMStub([notificationMock postNotificationName:[OCMArg any] object:[OCMArg any]]);
    
    NSData *data = [@"{" dataUsingEncoding:NSUTF8StringEncoding];

    BackendService *backend = [BackendService new];
    [backend connection:nil didReceiveResponse:nil];
    [backend connection:nil didReceiveData:data];
    [backend connectionDidFinishLoading:nil];
    
    OCMVerify([notificationMock postNotificationName:[OCMArg isEqual:@"register_fail"]
                                              object:[OCMArg isEqual:@"Incorrect JSON file."]]);
}

-(void)testIncorrectResponse2 {
    
    id notificationMock = OCMClassMock([NSNotificationCenter class]);
    OCMStub([notificationMock defaultCenter]).andReturn(notificationMock);
    OCMStub([notificationMock postNotificationName:[OCMArg any] object:[OCMArg any]]);
    
    NSData *data = [@"{}" dataUsingEncoding:NSUTF8StringEncoding];
    
    BackendService *backend = [BackendService new];
    [backend connection:nil didReceiveResponse:nil];
    [backend connection:nil didReceiveData:data];
    [backend connectionDidFinishLoading:nil];
    
    OCMVerify([notificationMock postNotificationName:[OCMArg isEqual:@"register_fail"]
                                              object:[OCMArg isEqual:@"Incorrect JSON file."]]);
}


-(void)testIncorrectResponse3 {
    
    // Number instead of a string for access_token.
    
    id notificationMock = OCMClassMock([NSNotificationCenter class]);
    OCMStub([notificationMock defaultCenter]).andReturn(notificationMock);
    OCMStub([notificationMock postNotificationName:[OCMArg any] object:[OCMArg any]]);
    
    NSData *data = [@"{\"access_token\": 123}" dataUsingEncoding:NSUTF8StringEncoding];
    
    BackendService *backend = [BackendService new];
    [backend connection:nil didReceiveResponse:nil];
    [backend connection:nil didReceiveData:data];
    [backend connectionDidFinishLoading:nil];
    
    OCMVerify([notificationMock postNotificationName:[OCMArg isEqual:@"register_fail"]
                                              object:[OCMArg isEqual:@"Incorrect JSON file."]]);
}


-(void)testIncorrectResponse4 {
    
    // Incorrect object type in JSON.
    
    id notificationMock = OCMClassMock([NSNotificationCenter class]);
    OCMStub([notificationMock defaultCenter]).andReturn(notificationMock);
    OCMStub([notificationMock postNotificationName:[OCMArg any] object:[OCMArg any]]);
    
    NSData *data = [@"[\"access_token\", 123]" dataUsingEncoding:NSUTF8StringEncoding];
    
    BackendService *backend = [BackendService new];
    [backend connection:nil didReceiveResponse:nil];
    [backend connection:nil didReceiveData:data];
    [backend connectionDidFinishLoading:nil];
    
    OCMVerify([notificationMock postNotificationName:[OCMArg isEqual:@"register_fail"]
                                              object:[OCMArg isEqual:@"Incorrect JSON file."]]);
}


-(void)testConnectionError {

    id notificationMock = OCMClassMock([NSNotificationCenter class]);
    OCMStub([notificationMock defaultCenter]).andReturn(notificationMock);
    OCMStub([notificationMock postNotificationName:[OCMArg any] object:[OCMArg any]]);
    
    BackendService *backend = [BackendService new];
    [backend connection:nil didFailWithError:nil];
    
    OCMVerify([notificationMock postNotificationName:[OCMArg isEqual:@"register_fail"]
                                              object:[OCMArg isEqual:@"No internet connection."]]);

}

-(void)testCorrectInitialization {
    
    NSData *data = [@"{" dataUsingEncoding:NSUTF8StringEncoding];
    
    BackendService *backend = [BackendService new];
    [backend connection:nil didReceiveResponse:nil];
    
    XCTAssertNotNil(backend.data);
}


-(void)testDataSetCorrectly {
    
    NSData *data = [@"{" dataUsingEncoding:NSUTF8StringEncoding];
    
    BackendService *backend = [BackendService new];
    [backend connection:nil didReceiveResponse:nil];
    [backend connection:nil didReceiveData:data];
    
    XCTAssert([backend.data isEqualToData:data]);
}

@end
