//
//  TestCompositionTests.m
//  TestCompositionTests
//
//  Created by Vladimir Obrizan on 12/7/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import <XCTest/XCTest.h>

#import <OCMock/OCMock.h>
#import "ViewController.h"

@interface TestCompositionTests : XCTestCase

@end

@implementation TestCompositionTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


-(void) testRegister {
    
    // 1. Set-up.
    id classMock = OCMClassMock([NSURLConnection class]);
    OCMStub([classMock connectionWithRequest:[OCMArg any] delegate:[OCMArg any]]);
    
    // 2. Stimuli.
    ViewController *vc = (ViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    NSDictionary *expected = @{@"login": @"obrizan",
                               @"password": @"asdfasdf1"};
    
    vc.login.text = expected[@"login"];
    vc.password.text = expected[@"password"];
    [vc registerTapped];
    OCMArg *checkBlock = [OCMArg checkWithBlock:^BOOL(id obj)
                          {
                              NSURLRequest *request = (NSURLRequest *)obj;
                              NSData *body = request.HTTPBody;
                              
                              NSError *e = nil;
                              
                              NSDictionary *d = [NSJSONSerialization JSONObjectWithData:body options:0 error:&e];
                              
                              return [expected isEqual:d] && [request.HTTPMethod isEqualToString:@"POST"];
                          }];
    
    // 3. Verify.
    OCMVerify([classMock connectionWithRequest:checkBlock delegate:[OCMArg any]]);
}


-(void) testNotRegister {
    
    // 1. Set-up.
    id classMock = OCMClassMock([NSURLConnection class]);
    OCMReject([classMock connectionWithRequest:[OCMArg any] delegate:[OCMArg any]]);
    
    // 2. Stimuli.
    ViewController *vc = (ViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    NSDictionary *expected = @{@"login": @"obrizan",
                               @"password": @"asdfasdf"};
    
    vc.login.text = expected[@"login"];
    vc.password.text = expected[@"password"];
    [vc registerTapped];
    
    // 3. Проверка не нужна, т. к. OCMReject выкинет исключение, если метод будет вызван.
    
}

@end
