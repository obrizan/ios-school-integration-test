//
//  ViewController.m
//  TestComposition
//
//  Created by Vladimir Obrizan on 12/7/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import "ViewController.h"

#import "UserService.h"

@interface ViewController () {
    UserService *_userService;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _userService = [UserService new];
    // Do any additional setup after loading the view, typically from a nib.
}

-(BOOL)isValidForm {
    
    return self.password.text.length > 8;
}

-(IBAction)registerTapped {
    
    if ([self isValidForm]) {
        [_userService createUser:self.login.text password:self.password.text];
    }
}

@end
