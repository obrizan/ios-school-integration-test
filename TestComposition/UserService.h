//
//  UserService.h
//  TestComposition
//
//  Created by Vladimir Obrizan on 12/7/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserService : NSObject

-(void)createUser:(NSString *)login password:(NSString *)password;

@end
