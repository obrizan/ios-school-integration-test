//
//  ViewController.h
//  TestComposition
//
//  Created by Vladimir Obrizan on 12/7/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *login;
@property (weak, nonatomic) IBOutlet UITextField *password;


-(IBAction)registerTapped;

@end

