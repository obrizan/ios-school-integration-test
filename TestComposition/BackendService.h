//
//  BackendService.h
//  TestComposition
//
//  Created by Vladimir Obrizan on 12/7/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "User.h"

@interface BackendService : NSObject<NSURLConnectionDataDelegate>

@property (nonatomic, readonly) NSData *data;

-(void)createUser:(User *)user;

@end
