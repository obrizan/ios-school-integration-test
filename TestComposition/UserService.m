//
//  UserService.m
//  TestComposition
//
//  Created by Vladimir Obrizan on 12/7/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import "UserService.h"

#import "BackendService.h"

@interface UserService() {
    BackendService *_backend;
}

@end

@implementation UserService


-(void)createUser:(NSString *)login password:(NSString *)password {
    
    _backend = [BackendService new];
    
    User *u = [User new];
    u.login = login;
    u.password = password;
    
    [_backend createUser:u];
}

@end
