//
//  BackendService.m
//  TestComposition
//
//  Created by Vladimir Obrizan on 12/7/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import "BackendService.h"

@implementation BackendService {
    User *_user;
    NSMutableData *_data;
    NSURLConnection *_connection;
}

-(void)createUser:(User *)user {
    
    _user = user;
    
    NSURL *url = [NSURL URLWithString:@"http://example.com"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSError *e = nil;
    NSDictionary *dic = @{
                        @"password": user.password,
                        @"login": user.login
                        };
    NSData *d = [NSJSONSerialization dataWithJSONObject:dic
                                                options:NSJSONWritingPrettyPrinted
                                                  error:&e];
    request.HTTPBody = d;
    
    _connection = [NSURLConnection connectionWithRequest:request delegate:self];
}



-(void)connection:(NSURLConnection *)c didReceiveResponse:(nonnull NSURLResponse *)response {
    
    _data = [NSMutableData data];
}



-(void)connection:(NSURLConnection *)c didReceiveData:(nonnull NSData *)data {
    
    [_data appendData:data];
}


-(void)connection:(NSURLConnection *)c didFailWithError:(nonnull NSError *)error {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"register_fail" object:@"No internet connection."];
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection {

    NSString *access_token = nil;
    
    @try {
    NSError *e = nil;
    NSDictionary *d = [NSJSONSerialization JSONObjectWithData:_data options:0 error:&e];
    

    access_token = d[@"access_token"];
    
    if (e || ![access_token isKindOfClass:[NSString class]]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"register_fail" object:@"Incorrect JSON file."];
        
        return;
    }
    }
    @catch (NSException *e) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"register_fail" object:@"Incorrect JSON file."];
    }
    
    
    [[NSUserDefaults standardUserDefaults] setObject:access_token forKey:@"access_token"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"register" object:access_token];
    
}



@end
